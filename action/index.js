module.exports = function (app) {
    app.actions = {};
    app.actions.bills = require('./bills')(app);
    app.actions.products = require('./products')(app);
    app.actions.users = require('./users')(app);
    app.actions.banners = require('./banners')(app);
    app.actions.newsletters = require('./newsletters')(app);
    app.actions.messages = require('./messages')(app);
};
