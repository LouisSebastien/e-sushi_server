module.exports = function (app) {
    return function (req, res, next) {
        var user = app.model.user;
        user.findOne({mail: req.body.mail}, function (err, result) {
            if (err) {
                return res.status(500).send({error: err});
            }
            else {
                if (result) {
                    if (app.bcrypt.compareSync(req.body.password, result.password)) {
                        return res.status(201).send({_id: result._id});
                    }
                    else {
                        return res.status(401).send({error: "User not found"});
                    }
                }
                else {
                    return res.status(401).send({error: "User not found"});
                }
            }
        });
    }
};