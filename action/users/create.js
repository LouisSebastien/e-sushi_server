module.exports = function (app) {
    return function (req, res, next) {
        var user = new app.model.user({
            mail: req.body.mail,
            firstname: req.body.firstname,
            lastname: req.body.lastname,
            password: req.body.password,
            birth: req.body.birth,
            creation: req.body.creation,
            points: req.body.points,
            gifts: req.body.gifts
        });
        user.save(function (err, result) {
            if (err) {
                if(err.code === 11000){
                    return res.status(409).send({error: "Mail already exists"});
                }
                return res.status(500).send({error: err});
            }
            else {
                if (result) {
                    return res.status(201).send({_id: result._id});
                }
                else {
                    return res.status(500).send({error: "User creation failed"});
                }
            }
        });
    }
};