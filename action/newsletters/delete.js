module.exports = function (app) {
    return function (req, res) {
        var newsletter = app.model.newsletter;
        var mail = req.body.mail;

        newsletter.remove({mail: mail}, function (err) {
            if (err) {
                return res.status(500).send({error: err});
            }
            else {
                res.status(200).send({});
            }
        });
    }
};