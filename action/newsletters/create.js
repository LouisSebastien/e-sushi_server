module.exports = function (app) {
    return function (req, res, next) {
        var newsletter = new app.model.newsletter({
            mail: req.body.mail
        });
        newsletter.save(function (err, result) {
            if (err) {
                return res.status(500).send({error: err});
            }
            else {
                if (result) {
                    return res.status(201).send({result: result});
                }
                else {
                    return res.status(500).send({error: "Newsletter subscription failed"});
                }
            }
        });
    }
};