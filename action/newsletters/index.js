module.exports = function(app){
    return {
        create: require('./create')(app),
        delete: require('./delete')(app),
        list: require('./list')(app)
    };
};