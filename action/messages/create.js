module.exports = function (app) {
    return function (req, res, next) {
        var message = new app.model.message({
            mail: req.body.mail
        });
        message.save(function (err, result) {
            if (err) {
                return res.status(500).send({error: err});
            }
            else {
                if (result) {
                    return res.status(201).send({result: result});
                }
                else {
                    return res.status(500).send({error: "Message creation failed"});
                }
            }
        });
    }
};