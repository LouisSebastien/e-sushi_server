module.exports = function (app) {
    return function (req, res) {
        var bill =  app.model.bill;
        var billId = req.params.billId;

        bill.updateOne({_id: billId}, req.body,  { runValidators: true }, function (err, result) {
            if(err){
                return res.status(500).send({error: err});
            }
            else{
                if (result!==null) {
                    res.send({});
                }
                else
                    res.send({error: "Bill update failed"});
            }
        });

    }
};