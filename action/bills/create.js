module.exports = function (app) {
    return function (req, res, next) {
        var bill = new app.model.bill({
            user: req.body.user,
            items: req.body.items,
            from: req.body.from,
            paid: req.body.paid
        });
        bill.save(function (err, result) {
            if (err) {
                return res.status(500).send({error: err});
            }
            else {
                if (result) {
                    return res.status(201).send({_id: result._id});
                }
                else {
                    return res.status(500).send({error: "Bill creation failed"});
                }
            }
        });
    }
};