module.exports = function (app) {
    return function (req, res) {
        var bill = app.model.bill;
        var billId = req.params.billId ;

        bill.remove({_id: billId }, function (err) {
            if (err) {
                return res.status(500).send({error: err});
            }
            else {
                res.status(200).send({});
            }
        });
    }
};