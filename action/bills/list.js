module.exports = function (app) {
    return function (req, res) {
        var bill =  app.model.bill;
        var options = {};
        if(req.query.paid != undefined){
            options.paid = req.query.paid;
        }
        if(req.query.from != undefined){
            options.from =  req.query.from;
        }
        bill.find(options,function (err,result) {
            if(err){
                return res.status(500).send({error: err});
            }
            else{
                if (result) {
                    res.send(result)
                }
                else
                    res.status(404).send({error: 'Resource not found'});
            }
        });

    }
};