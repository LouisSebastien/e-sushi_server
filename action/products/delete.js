module.exports = function (app) {
    return function (req, res) {
        var product = app.model.product;
        var productId = req.params.productId ;

        product.remove({_id: productId}, function (err) {
            if (err) {
                return res.status(500).send({error: err});
            }
            else {
                res.status(200).send({});
            }
        });
    }
};