module.exports = function (app) {
    return function (req, res) {
        var product =  app.model.product;
        var productId = req.params.productId;

        product.update({_id: productId}, req.body, function (err, result) {
            if(err){
                return res.status(500).send({error: err});
            }
            else{
                if (result!=null) {
                    res.send({});
                }
                else
                    res.send({error: "Product update failed"});
            }
        });

    }
};