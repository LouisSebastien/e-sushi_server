module.exports = function (app) {
    return function (req, res) {
        var product =  app.model.product;
        product.find({},function (err,result) {
            if(err){
                return res.status(500).send({error: err});
            }
            else{
                if (result) {
                    console.log(result)
                    res.send(result)
                }
                else
                    res.status(404).send({error: 'Resource not found'});
            }
        });

    }
};