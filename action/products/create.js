module.exports = function (app) {
    return function (req, res, next) {
        var product = new app.model.product({
            title: req.body.title,
            description: req.body.description,
            categoriesId: req.body.categoriesId,
            image: req.body.image,
            unitPrice: req.body.unitPrice,
            new: req.body.new,
            creation: req.body.creation,
            visible: req.body.visible,
            available: req.body.available,
            pieces: req.body.pieces,
            volume: req.body.volume
        });
        product.save(function (err, result) {
            if (err) {
                return res.status(500).send({error: err});
            }
            else {
                if (result) {
                    console.log(product.title + " created");
                    res.status(201).send({_id: result._id});
                }
                else
                    res.status(500).send({error: "Product creation failed"});
            }
        });
    }
};