'use strict';
module.exports = function (app) {
    app.bcrypt = require('bcrypt');
    app.BCRYPT_SALT_WORK_FACTOR = 10;

    var userSchema = app.mongoose.Schema(
        {
            mail: {
                type: String,
                required: true,
                unique: true
            },
            firstname: {
                type: String,
                required: true
            },
            lastname: {
                type: String,
                required: true
            },
            password: {
                type: String,
                required: true
            },
            birth: {
                day: {
                    type: Number,
                    required: true
                },
                month: {
                    type: Number,
                    required: true
                },
                year: {
                    type: Number,
                    required: true
                }
            },
            creation: {
                type: String,
                    default: new Date().getTime()
            },
            points: {
                type: Number,
                default: 0
            },
            gifts: {
                type: [app.mongoose.Schema.Types.Mixed],
                default: []
            }
        });

    userSchema.pre('save', function (next) {
        var user = this;

        app.bcrypt.genSalt(app.BCRYPT_SALT_WORK_FACTOR, function (err, salt) {
            if (err) {
                return next(err);
            }

            app.bcrypt.hash(user.password, salt, function (err, hash) {
                if (err) {
                    return next(err);
                }

                user.password = hash;
                next();
            });
        })
    });

    var user = app.mongoose.model('user', userSchema);
    return user;
};