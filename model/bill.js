'use strict';
module.exports = function (app) {
    var billSchema = app.mongoose.Schema(
        {
            user: {
                firstname: {
                    type: String
                },
                lastname: {
                    type: String
                },
                mail: {
                    type: String
                },
                phone: {
                    type: String
                }
            },
            items: [{
                _id: {
                    type: String,
                    required: true
                },
                title: {
                    type: String,
                    required: true
                },
                quantity: {
                    type: Number,
                    required: true
                },
                unitPrice: {
                    type: Number,
                    required: true
                },
                gift: {
                    type: Boolean,
                    default: false
                }
            }],
            from: {
                type: String,
                enum: ['Seat', 'Takeaway', 'Website', 'Phone'],
                required: true
            },
            paid: {
                type: Boolean,
                default: false
            },
            date: {
                type: Date,
                default: new Date().getTime()
            },
            number: {
                type: Number,
                default: 0
            }
        });

    billSchema.pre('save', function (next) {

        var doc = this;
        bill.findOne({}, {}, {sort: {number: -1}}, function (error, res) {
            if (res) {
                doc.number = res.number + 1;
            }
            next();
        });
    });

    var bill = app.mongoose.model('bill', billSchema);
    return bill;
};