'use strict';
module.exports = function (app) {
    var newsletterSchema = app.mongoose.Schema(
        {
            mail: {
                type: String,
                required: true,
                unique: true
            }
        });
    var newsletter = app.mongoose.model('newsletter', newsletterSchema);
    return newsletter;
};