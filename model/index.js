var mongoose = require('mongoose');

var uri = 'mongodb://heroku_20b08bpz:114narsn5c3ed7nhaataseo0oq@ds121295.mlab.com:21295/heroku_20b08bpz';

module.exports = function (app) {
    app.mongoose = mongoose.connect(uri,function (err,db) {
        if (err)
            console.log('Unable to connect to the mongoDB server. Error: ',err);
        else
            console.log('Connection established to', uri);
    });
    app.model = {};
    app.model.bill = require('./bill')(app);
    app.model.product = require('./product')(app);
    app.model.user = require('./user')(app);
    app.model.newsletter= require('./newsletter')(app);
    app.model.banner = require('./banner')(app);
    app.model.message = require('./message')(app);
};