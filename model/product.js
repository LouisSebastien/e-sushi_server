'use strict';
module.exports = function (app) {
    var productSchema = app.mongoose.Schema(
        {
            title: {
                type: String,
                required: true
            },
            description: {
                type: String,
                required: true
            },
            categoriesId: {
              type: [String],
              default: []
            },
            image: {
                type: String,
                required: true
            },
            unitPrice: {
                type: Number,
                required: true
            },
            new:{
                type: Boolean,
                default: false
            },
            creation:{
                type: Date,
                default: Date.now()
            },
            visible:{
                type: Boolean,
                default: false
            },
            available:{
                type: Boolean,
                default: true
            },
            pieces:{
                type: Number,
                default: 1
            },
            volume:{
                type: Number
            }
        });
    var product = app.mongoose.model('product', productSchema);
    return product;
};