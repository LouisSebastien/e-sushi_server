'use strict';
module.exports = function (app) {
    var bannerSchema = app.mongoose.Schema(
        {
            image: {
                type: String,
                required: true
            },
            title: {
                type: String,
                required: true
            },
            description: {
                type: String,
                required: true
            },
            url: {
                type: String,
                required: true
            }
        });
    var banner = app.mongoose.model('banner', bannerSchema);
    return banner;
};