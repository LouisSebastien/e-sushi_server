'use strict';
module.exports = function (app) {
    var messageSchema = app.mongoose.Schema(
        {
            mail: {
                type: String
            },
            content: {
                type: String
            }
        });
    var message = app.mongoose.model('message', messageSchema);
    return message;
};