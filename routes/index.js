module.exports = function (app) {
    app.use('/bills', require('./bills')(app));
    app.use('/products', require('./products')(app));
    app.use('/users', require('./users')(app));
    app.use('/banners', require('./banners')(app));
    app.use('/newsletters', require('./newsletters')(app));
    app.use('/messages', require('./messages')(app));
};