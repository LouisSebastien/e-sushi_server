var express = require('express');
var router = express.Router();
var bodyParser = require('body-parser').json();

module.exports = function (app) {
    router.get('/',
       // app.oauth.authorise(),
        app.actions.newsletters.list
    );
    router.post('/',
        app.actions.newsletters.create
    );
    router.delete('/',
        app.actions.newsletters.delete
    );
    return router;
};