var express = require('express');
var router = express.Router();
var bodyParser = require('body-parser').json();

module.exports = function (app) {
    router.get('/',
       // app.oauth.authorise(),
        app.actions.products.list
    );
    router.post('/',
        bodyParser,
        app.actions.products.create
    );
    router.put('/:productId',
        bodyParser,
        app.actions.products.update
    );
    router.delete('/:productId',
        app.actions.products.delete
    );
    return router;
};