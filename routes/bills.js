var express = require('express');
var router = express.Router();
var bodyParser = require('body-parser').json();

module.exports = function (app) {
    router.get('/',
        // app.oauth.authorise(),
        app.actions.bills.list
    );
    router.post('',
        bodyParser,
        app.actions.bills.create
    );
    router.put('/:billId',
        bodyParser,
        app.actions.bills.update
    );
    router.delete('/:billId',
        app.actions.bills.delete
    );
    return router;
};