var express = require('express');
var router = express.Router();
var bodyParser = require('body-parser').json();

module.exports = function (app) {
    router.get('/',
       // app.oauth.authorise(),
        app.actions.messages.list
    );
    router.post('/',
        app.actions.messages.create
    );
    return router;
};