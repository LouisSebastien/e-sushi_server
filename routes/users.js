var express = require('express');
var router = express.Router();
var bodyParser = require('body-parser').json();

module.exports = function (app) {
    router.get('/',
       // app.oauth.authorise(),
        app.actions.users.list
    );
    router.post('',
        bodyParser,
        app.actions.users.create
    );
    router.post('/login',
        bodyParser,
        app.actions.users.login
    );
    router.put('/:userId',
        bodyParser,
        app.actions.users.update
    );
    router.delete('/:userId',
        app.actions.users.delete
    );
    return router;
};